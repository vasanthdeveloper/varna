import { RootCommandData } from '../interfaces'

export default function makeFooter(appData: RootCommandData, styles): string {
    if (appData.license == null && appData.website == null) return ''

    return `${styles.newIndentedLine}`
        .concat(
            appData.license
                ? `Licensed under ${styles.license(appData.license)}`
                : '',
        )
        .concat(styles.newIndentedLine)
        .concat(
            appData.website ? `Visit ${styles.website(appData.website)}` : '',
        )
}

//   ___    __________   |  Vasanth Developer (Vasanth Srivatsa)
//   __ |  / /___  __ \  |  ------------------------------------------------
//   __ | / / __  / / /  |  https://github.com/vasanthdeveloper/varna.git
//   __ |/ /  _  /_/ /   |
//   _____/   /_____/    |  Entryfile for varna project
//                       |

import RootCommand from './classes/RootCommand'
import parseCommand from './parse/index'

export function app(appName: string): RootCommand {
    return new RootCommand(appName)
}

export function parse(
    root: RootCommand,
    sourceOptions: string[] = process.argv.splice(2),
): void {
    parseCommand(root.appData, sourceOptions)
}

// This is the class that handles everything!

import {
    RootCommandData,
    OptionData,
    SynopsisData,
    ArgumentData,
} from '../interfaces'
import Command from './Command'
import options from '../options'
import showHelp from '../help/index'
import commands from '../commands'
import args from '../arguments'

export default class RootCommand {
    appData: RootCommandData

    constructor(appName: string) {
        // check if appName was provided
        if (!appName) throw Error('No application name was provided.')

        // initialize a new RootCommandData object
        this.appData = {
            name: appName.toString().toLowerCase(),
            license: null,
            website: null,
            description: null,
            argument: {
                multiple: false,
                type: null,
                value: null,
            },
            func: () => {
                showHelp(this.appData, null)
                process.exit(0)
            },
            options: [
                {
                    name: 'help',
                    alias: 'h',
                    description: 'Show the help message and terminate.',
                    type: Boolean,
                    value: null,
                    multiple: false,
                },
            ],
            commands: [],
            synopsis: [],
            version: {
                value: null,
                func: null,
            },
            config: {
                colored: true,
                padding: 1,
                indentation: 3,
                maxWidth: 60,
            },
        }
    }

    name(name: string): this {
        this.appData.name = name
        return this
    }

    description(desc: string): this {
        if (!desc) throw Error('A description string was not provided.')
        this.appData.description = desc
        return this
    }

    license(license: string): this {
        this.appData.license = license
        return this
    }

    website(link: string): this {
        this.appData.website = link
        return this
    }

    version(
        ver: string,
        showFlag = true,
        func = () => {
            console.log(this.appData.name, this.appData.version.value)
        },
    ): this {
        if (!ver) throw Error('A version string was not provided.')

        this.appData.version.value = ver.toString()
        this.appData.version.func = func
        const versionFlag = this.appData.options.find(
            option => option.name == 'version',
        )

        if (showFlag == true && !versionFlag) {
            this.appData.options.unshift({
                name: 'version',
                alias: 'V',
                description: 'Show the version information and terminate.',
                type: Boolean,
                value: null,
                multiple: false,
            })
        } else if (showFlag == false && versionFlag) {
            this.appData.options.splice(
                this.appData.options.indexOf(versionFlag),
                1,
            )
        }

        return this
    }

    argument(arg: ArgumentData): this {
        args.addArgument(arg, this.appData)
        return this
    }

    option(option: OptionData): this {
        options.addOption(option, this.appData)
        return this
    }

    command(name: string, commandFn: (command: Command) => Command): this {
        commands.addCommand(this.appData, name, commandFn, this.appData)
        return this
    }

    synopsis(synopsis: string): this {
        const newSynopsis: SynopsisData = {
            id: this.appData.synopsis.length + 1,
            text: synopsis,
        }
        this.appData.synopsis.push(newSynopsis)
        return this
    }

    func(
        func: (
            rootCommand: RootCommandData,
            globalOptions: OptionData[],
        ) => void,
    ): this {
        this.appData.func = func
        return this
    }
}

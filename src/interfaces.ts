export interface ArgumentData {
    type: any
    value: any
    multiple?: boolean
    typeLabel?: string
}

export interface OptionData extends ArgumentData {
    name: string
    alias?: string
    description: string
}

export interface SynopsisData {
    id: number
    text: string
}

export interface CommandData {
    name: string
    description: string
    options?: OptionData[]
    synopsis?: SynopsisData[]
    subCommands?: CommandData[]
    argument: ArgumentData
    func: (
        command: CommandData,
        commandOptions: OptionData[],
        globalOptions: OptionData[],
    ) => void
}

export interface RootCommandData {
    name: string
    license: string
    website: string
    description: string
    options: OptionData[]
    commands: CommandData[]
    synopsis: SynopsisData[]
    argument: ArgumentData
    func: (rootCommand: RootCommandData, globalOptions: OptionData[]) => void
    version: {
        value: string
        func: () => void
    }
    config: {
        colored: boolean
        padding: number
        indentation: number
        maxWidth: number
    }
}

import { CommandData, RootCommandData } from '../interfaces'
import { getValOfOption } from './globalOptions'

export function getCommandValue(
    existsAt: number,
    command: CommandData | RootCommandData,
    sourceOptions: string[],
): void {
    let value: any

    try {
        value = getValOfOption(existsAt, sourceOptions, command.name)
    } catch (e) {
        if (!command.argument.value) {
            throw e
        } else {
            value = command.argument.value
        }
    }

    // validate what we got
    const valid = command.argument.type(value)
    if (!valid)
        throw Error(`Invalid value "${value}" provided for ${command.name}`)

    command.argument.value = valid
}

export default function parseCommands(
    commands: CommandData[],
    sourceOptions: string[],
): CommandData {
    let returnable: CommandData = null

    commands.forEach(cmd => {
        const existsAt = sourceOptions.indexOf(cmd.name)
        if (existsAt != -1) {
            sourceOptions.splice(existsAt, 1)
            if (cmd.subCommands.length > 0) {
                const nextCommand = parseCommands(
                    cmd.subCommands,
                    sourceOptions,
                )
                if (nextCommand) {
                    if (nextCommand.argument.type)
                        getCommandValue(existsAt, nextCommand, sourceOptions)
                    returnable = nextCommand
                } else {
                    if (cmd.argument.type)
                        getCommandValue(existsAt, cmd, sourceOptions)
                    returnable = cmd
                }
            } else {
                if (cmd.argument.type)
                    getCommandValue(existsAt, cmd, sourceOptions)
                returnable = cmd
            }
        }
    })

    return returnable
}

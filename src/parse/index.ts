// This file combines all the other files in this directory
// and fires the final callback function with the parsed data.

import { RootCommandData } from '../interfaces'

import parseGlobalOptions from './globalOptions'
import parseCommands, { getCommandValue } from './command'
import parseCommandOptions from './commandOptions'
import showHelp from '../help'

export default function parse(
    appData: RootCommandData,
    sourceOptions: string[],
): void {
    const globalOptions = parseGlobalOptions(appData, sourceOptions)
    const command = parseCommands(appData.commands, sourceOptions)
    const commandOptions = parseCommandOptions(appData, command, sourceOptions)

    // get the rootCommand's value
    if (!command && appData.argument.type)
        getCommandValue(0, appData, sourceOptions)

    // handle any left-over elements in sourceOptions
    if (sourceOptions.length > 0)
        throw Error(`Unrecognized value "${sourceOptions[0]}".`)

    // handle the --help flag
    const help = globalOptions.find(option => option.name == 'help')
    if (help.value) {
        showHelp(appData, command, false)
        process.exit(0)
    }

    // handle the --version flag
    const version = globalOptions.find(option => option.name == 'version')
    if (version && version.value == true) {
        appData.version.func()
        process.exit(0)
    }

    // fire the appropriate function according to the command we parsed
    if (command) {
        command.func(command, commandOptions, globalOptions)
    } else {
        appData.func(appData, globalOptions)
    }
}

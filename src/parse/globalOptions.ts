import { RootCommandData, OptionData } from '../interfaces'

export function parseSingleOption(
    option: OptionData,
    index: number,
    sourceOptions: string[],
): OptionData {
    if (option.type == Boolean) {
        option.value = true
        return option
    } else {
        const value = getValOfOption(index, sourceOptions)

        // validate the value we got
        const valid = option.type(value)
        if (!valid)
            throw Error(
                `Invalid value "${value}" provided for ${sourceOptions[index]}`,
            )

        option.value = valid
        return option
    }
}

// getValOfOption will get the value of an option from sourceOptions
export function getValOfOption(
    existsAt: number,
    sourceOptions: string[],
    cmdName?: string,
): string | undefined {
    // the error thrown when a value for option wasn't found
    const notFoundError = Error(
        `Expected a value for ${cmdName ? cmdName : sourceOptions[existsAt]}.`,
    )

    const point = cmdName ? existsAt : existsAt + 1

    // check if sourceOptions actually has that many elements
    if (sourceOptions.length > point) {
        // get and convert the sourceOptions element into a string
        // in case the user provided a different type
        // note that process.argv will always give you string[]
        const nextVal = sourceOptions[point].toString()

        // if the nextVal isn't null, undefined, false or any
        // negative value
        if (nextVal) {
            // check if it isn't another option
            if (nextVal.startsWith('-') == false) {
                return nextVal
            } else {
                throw notFoundError
            }
        } else {
            throw notFoundError
        }
    } else {
        throw notFoundError
    }
}

export function populateOptionsArray(
    options: OptionData[],
    optionsToBePopulated: OptionData[],
): OptionData[] {
    // loop through all possible options and add them to the returnable with default values
    options.forEach(option => {
        const exists = optionsToBePopulated.find(
            argv => argv.name == option.name,
        )
        if (!exists) {
            if (option.type == Boolean) {
                if (!option.value) option.value = false
            } else {
                if (option.value) {
                    // check if the default value is valid
                    // because I trust no one! ;)
                    const valid = option.type(option.value)
                    if (!valid)
                        throw Error(
                            `The default value "${option.value}" provided for ${option.name} is invalid.`,
                        )
                } else {
                    option.value = null
                }
            }

            // add it to the optionsToBePopulated list
            optionsToBePopulated.push(option)
        }
    })

    return optionsToBePopulated
}

export default function parseGlobalOptions(
    appData: RootCommandData,
    sourceOptions: string[],
): OptionData[] {
    // prepare the returnable constant that holds list of global options
    const globalOptions: OptionData[] = []
    const skipStrings: string[] = []

    // loop through all known global options
    appData.options.forEach(option => {
        let exists: number = option.alias
            ? sourceOptions.indexOf(`-${option.alias}`)
            : -1
        if (exists == -1) exists = sourceOptions.indexOf(`--${option.name}`)

        // now act accordingly if the user passed the following option or not
        if (exists > -1) {
            const parsed = parseSingleOption(option, exists, sourceOptions)
            skipStrings.push(sourceOptions[exists])
            globalOptions.push(parsed)
        }
    })

    // remove the processed options from sourceOptions
    skipStrings.forEach(skipStr => {
        sourceOptions.splice(sourceOptions.indexOf(skipStr), 1)
    })

    return populateOptionsArray(appData.options, globalOptions)
}

import Command from './classes/Command'
import { RootCommandData, CommandData } from './interfaces'

function addCommand(
    appData: RootCommandData,
    name: string,
    commandFn: (command: Command) => Command,
    command: RootCommandData | CommandData,
): void {
    // check if the developer submitted a command name
    // and the callback function that allows the developer to
    // modify the command.
    if (!name) throw Error('A command name is required.')
    if (!commandFn)
        throw Error(
            "A callback function which allows mutation of the command wasn't provided.",
        )

    // check if there already exists a command with that name
    const exists: CommandData =
        command == appData
            ? appData.commands.find(cmd => cmd.name == name)
            : command['subCommands'].find(cmd => cmd.name == name)
    if (exists)
        throw Error(
            `A command with name "${name}" already exists on ${
                command == appData ? 'root' : command.name
            }.`,
        )

    // create a new command class
    const createdCommand = new Command(name, appData)

    // pass the newly created command with our mutation callback function
    const finalCommand = commandFn(createdCommand)

    // check if a command was returned by the developer
    if (!finalCommand)
        throw Error("The callback function didn't return the mutated command.")

    // finally add it to our command
    if (command == appData) {
        appData.commands.push(finalCommand.commandData)
    } else {
        command['subCommands'].push(finalCommand.commandData)
    }
}

export default {
    addCommand,
}

import { CommandData, RootCommandData } from '../interfaces';
export default function makeCommands(appData: RootCommandData, commands: CommandData[], styles: any, short: boolean): string;
//# sourceMappingURL=commands.d.ts.map
"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var chalk_1 = __importDefault(require("chalk"));
var header_1 = __importDefault(require("./header"));
var commands_1 = __importDefault(require("./commands"));
var options_1 = __importDefault(require("./options"));
var footer_1 = __importDefault(require("./footer"));
function makeAdditionalBanner(appData, command, styles) {
    var message = "See \"" + appData.name + " " + (command ? styles.command(command.name + ' ') : '') + styles.option('--help') + "\" for full help.";
    if (command) {
        if (appData.commands.length > 4 ||
            command.subCommands.length > 4 ||
            appData.options.length > 5 ||
            command.options.length > 5) {
            return ("\n" + styles.newIndentedLine).concat(message ? message : '');
        }
        else {
            return '';
        }
    }
    else {
        if (appData.commands.length > 4 || appData.options.length > 5) {
            return ("\n" + styles.newIndentedLine).concat(message ? message : '');
        }
        else {
            return '';
        }
    }
}
function showHelp(appData, command, short) {
    if (short === void 0) { short = true; }
    var styles = {
        newLine: "\n" + ' '.repeat(appData.config.padding),
        newIndentedLine: "\n" + ' '.repeat(appData.config.padding + appData.config.indentation),
        header: appData.config.colored ? chalk_1.default.bold.white : chalk_1.default.bold,
        name: appData.config.colored ? chalk_1.default.bold : chalk_1.default,
        license: appData.config.colored ? chalk_1.default.bold.white : chalk_1.default.bold,
        website: appData.config.colored
            ? chalk_1.default.underline.bold
            : chalk_1.default.underline.bold,
        command: appData.config.colored ? chalk_1.default.blueBright : chalk_1.default,
        option: appData.config.colored ? chalk_1.default.yellowBright : chalk_1.default,
        typeLabel: appData.config.colored ? chalk_1.default.italic.cyanBright : chalk_1.default,
        defaultValue: appData.config.colored ? chalk_1.default.grey : chalk_1.default,
    };
    var helpString = ''
        .concat((0, header_1.default)(appData, styles))
        .concat(command
        ? (0, commands_1.default)(appData, command.subCommands, styles, short)
        : (0, commands_1.default)(appData, appData.commands, styles, short))
        .concat(command ? (0, options_1.default)(appData, command.options, styles, short) : '')
        .concat((0, options_1.default)(appData, appData.options, styles, short))
        .concat((0, footer_1.default)(appData, styles))
        .concat(short == true ? makeAdditionalBanner(appData, command, styles) : '');
    console.log(helpString);
}
exports.default = showHelp;
//# sourceMappingURL=index.js.map
import { ArgumentData, RootCommandData, CommandData } from './interfaces';
declare function addArgument(arg: ArgumentData, command: RootCommandData | CommandData): void;
declare const _default: {
    addArgument: typeof addArgument;
};
export default _default;
//# sourceMappingURL=arguments.d.ts.map
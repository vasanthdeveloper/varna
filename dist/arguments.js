"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function addArgument(arg, command) {
    if (!arg)
        throw Error('No argument object was passed.');
    if (!arg.type)
        throw Error('An argument type is required.');
    if (arg.type == Boolean)
        throw Error('A positional argument cannot be of type boolean.');
    if (command.argument.type)
        throw Error("An argument of type " + command.argument.type.name.toString() + " is already assigned to the command \"" + command.name + "\".");
    arg.multiple
        ? (command.argument.multiple = arg.multiple)
        : (command.argument.multiple = false);
    arg.value
        ? (command.argument.value = arg.value)
        : (command.argument.value = null);
    if (arg.typeLabel)
        command.argument.typeLabel = arg.typeLabel;
    command.argument.type = arg.type;
}
exports.default = {
    addArgument: addArgument,
};
//# sourceMappingURL=arguments.js.map
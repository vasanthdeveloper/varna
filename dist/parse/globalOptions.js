"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.populateOptionsArray = exports.getValOfOption = exports.parseSingleOption = void 0;
function parseSingleOption(option, index, sourceOptions) {
    if (option.type == Boolean) {
        option.value = true;
        return option;
    }
    else {
        var value = getValOfOption(index, sourceOptions);
        var valid = option.type(value);
        if (!valid)
            throw Error("Invalid value \"" + value + "\" provided for " + sourceOptions[index]);
        option.value = valid;
        return option;
    }
}
exports.parseSingleOption = parseSingleOption;
function getValOfOption(existsAt, sourceOptions, cmdName) {
    var notFoundError = Error("Expected a value for " + (cmdName ? cmdName : sourceOptions[existsAt]) + ".");
    var point = cmdName ? existsAt : existsAt + 1;
    if (sourceOptions.length > point) {
        var nextVal = sourceOptions[point].toString();
        if (nextVal) {
            if (nextVal.startsWith('-') == false) {
                return nextVal;
            }
            else {
                throw notFoundError;
            }
        }
        else {
            throw notFoundError;
        }
    }
    else {
        throw notFoundError;
    }
}
exports.getValOfOption = getValOfOption;
function populateOptionsArray(options, optionsToBePopulated) {
    options.forEach(function (option) {
        var exists = optionsToBePopulated.find(function (argv) { return argv.name == option.name; });
        if (!exists) {
            if (option.type == Boolean) {
                if (!option.value)
                    option.value = false;
            }
            else {
                if (option.value) {
                    var valid = option.type(option.value);
                    if (!valid)
                        throw Error("The default value \"" + option.value + "\" provided for " + option.name + " is invalid.");
                }
                else {
                    option.value = null;
                }
            }
            optionsToBePopulated.push(option);
        }
    });
    return optionsToBePopulated;
}
exports.populateOptionsArray = populateOptionsArray;
function parseGlobalOptions(appData, sourceOptions) {
    var globalOptions = [];
    var skipStrings = [];
    appData.options.forEach(function (option) {
        var exists = option.alias
            ? sourceOptions.indexOf("-" + option.alias)
            : -1;
        if (exists == -1)
            exists = sourceOptions.indexOf("--" + option.name);
        if (exists > -1) {
            var parsed = parseSingleOption(option, exists, sourceOptions);
            skipStrings.push(sourceOptions[exists]);
            globalOptions.push(parsed);
        }
    });
    skipStrings.forEach(function (skipStr) {
        sourceOptions.splice(sourceOptions.indexOf(skipStr), 1);
    });
    return populateOptionsArray(appData.options, globalOptions);
}
exports.default = parseGlobalOptions;
//# sourceMappingURL=globalOptions.js.map
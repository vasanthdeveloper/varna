"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getCommandValue = void 0;
var globalOptions_1 = require("./globalOptions");
function getCommandValue(existsAt, command, sourceOptions) {
    var value;
    try {
        value = (0, globalOptions_1.getValOfOption)(existsAt, sourceOptions, command.name);
    }
    catch (e) {
        if (!command.argument.value) {
            throw e;
        }
        else {
            value = command.argument.value;
        }
    }
    var valid = command.argument.type(value);
    if (!valid)
        throw Error("Invalid value \"" + value + "\" provided for " + command.name);
    command.argument.value = valid;
}
exports.getCommandValue = getCommandValue;
function parseCommands(commands, sourceOptions) {
    var returnable = null;
    commands.forEach(function (cmd) {
        var existsAt = sourceOptions.indexOf(cmd.name);
        if (existsAt != -1) {
            sourceOptions.splice(existsAt, 1);
            if (cmd.subCommands.length > 0) {
                var nextCommand = parseCommands(cmd.subCommands, sourceOptions);
                if (nextCommand) {
                    if (nextCommand.argument.type)
                        getCommandValue(existsAt, nextCommand, sourceOptions);
                    returnable = nextCommand;
                }
                else {
                    if (cmd.argument.type)
                        getCommandValue(existsAt, cmd, sourceOptions);
                    returnable = cmd;
                }
            }
            else {
                if (cmd.argument.type)
                    getCommandValue(existsAt, cmd, sourceOptions);
                returnable = cmd;
            }
        }
    });
    return returnable;
}
exports.default = parseCommands;
//# sourceMappingURL=command.js.map
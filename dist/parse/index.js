"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var globalOptions_1 = __importDefault(require("./globalOptions"));
var command_1 = __importStar(require("./command"));
var commandOptions_1 = __importDefault(require("./commandOptions"));
var help_1 = __importDefault(require("../help"));
function parse(appData, sourceOptions) {
    var globalOptions = (0, globalOptions_1.default)(appData, sourceOptions);
    var command = (0, command_1.default)(appData.commands, sourceOptions);
    var commandOptions = (0, commandOptions_1.default)(appData, command, sourceOptions);
    if (!command && appData.argument.type)
        (0, command_1.getCommandValue)(0, appData, sourceOptions);
    if (sourceOptions.length > 0)
        throw Error("Unrecognized value \"" + sourceOptions[0] + "\".");
    var help = globalOptions.find(function (option) { return option.name == 'help'; });
    if (help.value) {
        (0, help_1.default)(appData, command, false);
        process.exit(0);
    }
    var version = globalOptions.find(function (option) { return option.name == 'version'; });
    if (version && version.value == true) {
        appData.version.func();
        process.exit(0);
    }
    if (command) {
        command.func(command, commandOptions, globalOptions);
    }
    else {
        appData.func(appData, globalOptions);
    }
}
exports.default = parse;
//# sourceMappingURL=index.js.map
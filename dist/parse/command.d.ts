import { CommandData, RootCommandData } from '../interfaces';
export declare function getCommandValue(existsAt: number, command: CommandData | RootCommandData, sourceOptions: string[]): void;
export default function parseCommands(commands: CommandData[], sourceOptions: string[]): CommandData;
//# sourceMappingURL=command.d.ts.map
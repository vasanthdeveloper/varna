"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var options_1 = __importDefault(require("../options"));
var index_1 = __importDefault(require("../help/index"));
var commands_1 = __importDefault(require("../commands"));
var arguments_1 = __importDefault(require("../arguments"));
var RootCommand = (function () {
    function RootCommand(appName) {
        var _this = this;
        if (!appName)
            throw Error('No application name was provided.');
        this.appData = {
            name: appName.toString().toLowerCase(),
            license: null,
            website: null,
            description: null,
            argument: {
                multiple: false,
                type: null,
                value: null,
            },
            func: function () {
                (0, index_1.default)(_this.appData, null);
                process.exit(0);
            },
            options: [
                {
                    name: 'help',
                    alias: 'h',
                    description: 'Show the help message and terminate.',
                    type: Boolean,
                    value: null,
                    multiple: false,
                },
            ],
            commands: [],
            synopsis: [],
            version: {
                value: null,
                func: null,
            },
            config: {
                colored: true,
                padding: 1,
                indentation: 3,
                maxWidth: 60,
            },
        };
    }
    RootCommand.prototype.name = function (name) {
        this.appData.name = name;
        return this;
    };
    RootCommand.prototype.description = function (desc) {
        if (!desc)
            throw Error('A description string was not provided.');
        this.appData.description = desc;
        return this;
    };
    RootCommand.prototype.license = function (license) {
        this.appData.license = license;
        return this;
    };
    RootCommand.prototype.website = function (link) {
        this.appData.website = link;
        return this;
    };
    RootCommand.prototype.version = function (ver, showFlag, func) {
        var _this = this;
        if (showFlag === void 0) { showFlag = true; }
        if (func === void 0) { func = function () {
            console.log(_this.appData.name, _this.appData.version.value);
        }; }
        if (!ver)
            throw Error('A version string was not provided.');
        this.appData.version.value = ver.toString();
        this.appData.version.func = func;
        var versionFlag = this.appData.options.find(function (option) { return option.name == 'version'; });
        if (showFlag == true && !versionFlag) {
            this.appData.options.unshift({
                name: 'version',
                alias: 'V',
                description: 'Show the version information and terminate.',
                type: Boolean,
                value: null,
                multiple: false,
            });
        }
        else if (showFlag == false && versionFlag) {
            this.appData.options.splice(this.appData.options.indexOf(versionFlag), 1);
        }
        return this;
    };
    RootCommand.prototype.argument = function (arg) {
        arguments_1.default.addArgument(arg, this.appData);
        return this;
    };
    RootCommand.prototype.option = function (option) {
        options_1.default.addOption(option, this.appData);
        return this;
    };
    RootCommand.prototype.command = function (name, commandFn) {
        commands_1.default.addCommand(this.appData, name, commandFn, this.appData);
        return this;
    };
    RootCommand.prototype.synopsis = function (synopsis) {
        var newSynopsis = {
            id: this.appData.synopsis.length + 1,
            text: synopsis,
        };
        this.appData.synopsis.push(newSynopsis);
        return this;
    };
    RootCommand.prototype.func = function (func) {
        this.appData.func = func;
        return this;
    };
    return RootCommand;
}());
exports.default = RootCommand;
//# sourceMappingURL=RootCommand.js.map
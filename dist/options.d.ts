import { OptionData, RootCommandData, CommandData } from './interfaces';
declare function addOption(option: OptionData, command: RootCommandData | CommandData): void;
declare const _default: {
    addOption: typeof addOption;
};
export default _default;
//# sourceMappingURL=options.d.ts.map
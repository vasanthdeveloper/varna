"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Command_1 = __importDefault(require("./classes/Command"));
function addCommand(appData, name, commandFn, command) {
    if (!name)
        throw Error('A command name is required.');
    if (!commandFn)
        throw Error("A callback function which allows mutation of the command wasn't provided.");
    var exists = command == appData
        ? appData.commands.find(function (cmd) { return cmd.name == name; })
        : command['subCommands'].find(function (cmd) { return cmd.name == name; });
    if (exists)
        throw Error("A command with name \"" + name + "\" already exists on " + (command == appData ? 'root' : command.name) + ".");
    var createdCommand = new Command_1.default(name, appData);
    var finalCommand = commandFn(createdCommand);
    if (!finalCommand)
        throw Error("The callback function didn't return the mutated command.");
    if (command == appData) {
        appData.commands.push(finalCommand.commandData);
    }
    else {
        command['subCommands'].push(finalCommand.commandData);
    }
}
exports.default = {
    addCommand: addCommand,
};
//# sourceMappingURL=commands.js.map
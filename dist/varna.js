"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.parse = exports.app = void 0;
var RootCommand_1 = __importDefault(require("./classes/RootCommand"));
var index_1 = __importDefault(require("./parse/index"));
function app(appName) {
    return new RootCommand_1.default(appName);
}
exports.app = app;
function parse(root, sourceOptions) {
    if (sourceOptions === void 0) { sourceOptions = process.argv.splice(2); }
    (0, index_1.default)(root.appData, sourceOptions);
}
exports.parse = parse;
//# sourceMappingURL=varna.js.map